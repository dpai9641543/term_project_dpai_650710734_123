<!DOCTYPE html>
<html lang="th" class="isDesktop">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<title>Database</title>
</head>
<style>
  @import url('https://fonts.googleapis.com/css2?family=Prompt&display=swap');
  
  body {
    font-family: 'Bai Jamjuree', sans-serif;
    color: rgb(99, 40, 107);
    margin-top: 50px;
    margin-left: 50px;
  }
  .header {
  background-color: #e3cfe7;
  padding: 25px;
}
.text {
  font-family: 'Bai Jamjuree', sans-serif;
  color: rgb(69, 22, 77);
  margin-left: 10%;
  margin-right: 10%;
}
.button {
  font-family: 'Prompt', sans-serif;
  color: rgb(255, 255, 255);
  background-color: rgb(99, 40, 107);
}
.bg {
  background-color: #faf6fc;
}
</style>
<body>
<div class="header"></div>
<?php
   $servername = "db";
   $username = "devops";
   $password = "devops101";

   $dbhandle = mysqli_connect($servername, $username, $password);
   $selected = mysqli_select_db($dbhandle, "titanic");
   
   echo "Connected database server<br>";
   echo "Selected database";
?>
<div class="w3-container bg w3-center">
    <a href="index.html" class="btn w3-button w3-margin-bottom button"><i class="fa fa-home"></i> Profile</a>
    <a href="interested.html" class="btn w3-button w3-margin-bottom button"><i class="fa fa-heart"></i>  สิ่งที่ฉันสนใจ</a>
    <a href="about_su.html" class="btn w3-button w3-margin-bottom button"><i class="fa fa-map-marker"></i>  รอบรั้วศิลปากร</a>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
    crossorigin="anonymous"></script>
</body>
</html>
